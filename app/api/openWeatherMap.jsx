var axios = require('axios');

const OPEN_WEATHER_MAP_URL = 'http://api.openweathermap.org/data/2.5/weather?appid=487b87a2c9cbf684cc89b1e6d6717fcb&units=metric';
//api-key 487b87a2c9cbf684cc89b1e6d6717fcb

module.exports = {
  getTemp: function(city) {
      var encodedCity = encodeURIComponent(city);
      var requestUrl = `${OPEN_WEATHER_MAP_URL}&q=${encodedCity}`;

      return axios.get(requestUrl).then(function(res){
        if (res.cod && res.message) {
          throw new Error(res.message);
        } else {
          return res.data.main.temp;
        }
      }, function(err) {
        throw new Error(err.response.data.message);
      })
  }
}
