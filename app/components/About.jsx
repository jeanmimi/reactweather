var React = require('react');

var About = (props) => {
  return (
    <div>
      <h1 className="text-center page-title">About</h1>
      <p>This is a weather app built on React.
      I have built this for Complete React Web App Developer course
      </p>
      <p>
      Here are sme tools used
    </p>
    <ul>
      <li>
        <a href="https://facebook.github.io/react">React</a> - This is the JS framework used.
      </li>
      <li>
        <a href="http://openweathermap.org">Open Weather Map</a> - Wether API used to search weather data by city name.
      </li>
    </ul>
    </div>
  );
};

module.exports = About;
